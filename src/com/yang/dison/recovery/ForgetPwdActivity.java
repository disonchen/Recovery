package com.yang.dison.recovery;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.yang.dison.recovery.core.ui.RecoveryActivity;

public class ForgetPwdActivity extends RecoveryActivity {
	private Button verificationButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		initView();
	}
	
	private void initView() {
		setMainLayoutUseAnim(R.layout.activity_forgetpwd, false);
		setTopCenterText("忘记密码");
		showBottom(View.GONE);
		
		verificationButton = (Button) bindUI(R.id.forget_layout_verification_button, new OnClickListener() {
			@Override
			public void onClick(View v) {
				int verificationCode = createVerificationCode();
				verificationButton.setText(verificationCode + "");
				verificationButton.setTextSize(12);
			}
		});
	}
}
