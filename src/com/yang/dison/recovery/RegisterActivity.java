package com.yang.dison.recovery;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.yang.dison.recovery.core.ui.RecoveryActivity;

public class RegisterActivity extends RecoveryActivity {
	
	private Spinner typeSpinner;
	private List<String> data_list;
	private ArrayAdapter<String> arr_adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		initView();
	}
	
	private void initView() {
		setMainLayoutUseAnim(R.layout.activity_register, false);
		setTopCenterText("注册");
		showBottom(View.GONE);
		
		typeSpinner = (Spinner) bindUI(R.id.register_layout_type_spinner, null);
		//数据
        data_list = new ArrayList<String>();
        data_list.add("北京");
        data_list.add("上海");
        data_list.add("广州");
        data_list.add("深圳");
        
        //适配器
        arr_adapter= new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, data_list);
        //设置样式
        arr_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //加载适配器
        typeSpinner.setAdapter(arr_adapter);
	}
}
