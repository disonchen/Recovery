package com.yang.dison.recovery;

import android.os.Bundle;

import com.yang.dison.recovery.core.ui.RecoveryActivity;

public class RecycleActivity extends RecoveryActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setMainLayoutUseAnim(R.layout.activity_recycle, false);
	}
	
}
