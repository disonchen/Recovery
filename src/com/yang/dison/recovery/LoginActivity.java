package com.yang.dison.recovery;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.yang.dison.recovery.core.ui.RecoveryActivity;

public class LoginActivity extends RecoveryActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		initView();
	}
	
	private void initView() {
		setMainLayoutUseAnim(R.layout.activity_login, false);
		setTopCenterText("登录");
		showBottom(View.GONE);
		
		//忘记密码界面跳转
		bindUI(R.id.login_activity_forget_pwd_layout, new OnClickListener() {
			@Override
			public void onClick(View v) {
//				Intent intent = new Intent(LoginActivity.this, ForgetPwdActivity.class);
				Intent intent = new Intent(LoginActivity.this, ActivationActivity.class);
				startActivity(intent);
			}
		});
	}
}
