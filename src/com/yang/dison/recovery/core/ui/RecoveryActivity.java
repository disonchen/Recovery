package com.yang.dison.recovery.core.ui;

import java.util.HashMap;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yang.dison.recovery.R;

/**
 * 主Activity，设置一些共性的属性
 *
 * @author dison
 * @date   2015年11月5日
 */
public class RecoveryActivity extends Activity {
	//顶部菜单栏
	private LinearLayout topLayout;
	//底部导航栏
	private LinearLayout bottomLayout;
	
	//主界面
	private LinearLayout mainLayout;
	
	//顶部菜单栏的子控件
	private LinearLayout topLeftLayout;
	private LinearLayout topCenterLayout;
	private LinearLayout topRightLayout;
	private TextView topLeftTextView;
	private TextView topCenterTextView;
	private TextView topRightTextView;
	
	//底部导航栏的子控件
	private LinearLayout bottomIndexLayout;
	private LinearLayout bottomTypeLayout;
	private LinearLayout bottomRecycleLayout;
	private LinearLayout bottomMeLayout;
	
	//所有底部导航栏的layout
	private Map<String, LinearLayout> bottomItems = new HashMap<String, LinearLayout>();
	//当前Activity的名字
	private String currActivity = "MainActivity";
	//是否加载底部导航栏
	private boolean isShowBottom = true;
	
	@SuppressLint("UseSparseArrays")
	private Map<Integer, View> viewMap = new HashMap<Integer, View>();

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// 禁用ActionBar
		ActionBar actionBar = getActionBar();
		if (actionBar != null)
			actionBar.hide();
		// 设置xml界面
		setContentView(R.layout.activity_recovery);
		// 初始ui
		initUI();
//		topCenterLayout.add
	}
	
	/**
	 * 初始化界面的元素
	 */
	public void initUI() {
		topLayout = (LinearLayout) bindUI(R.id.top, null);
		bottomLayout = (LinearLayout) bindUI(R.id.bottom, null);
		mainLayout = (LinearLayout) bindUI(R.id.main_layout, null);
		topLeftLayout = (LinearLayout) bindUI(R.id.top_left_layout, new OnClickListener() {
			@Override
			public void onClick(View v) {
//				Toast.makeText(RecoveryActivity.this, "返回", Toast.LENGTH_SHORT).show();
				finish();
			}
		});
		topCenterLayout = (LinearLayout) bindUI(R.id.top_center_layout, null);
		topRightLayout = (LinearLayout) bindUI(R.id.top_right_layout, null);
		
		topLeftTextView = (TextView) bindUI(R.id.top_left_textview, null);
		@SuppressWarnings("deprecation")
		Drawable dw = getResources().getDrawable(R.drawable.icon_back);
		dw.setBounds(18, 2, 30, 20);
		topLeftTextView.setCompoundDrawables(dw, null, null, null);
		
		topCenterTextView = (TextView) bindUI(R.id.top_center_textview, null);
		topRightTextView = (TextView) bindUI(R.id.top_right_textview, null);
		
		//初始化底部
		if (isShowBottom) 
			initBottom();
	}
	
	private void initBottom() {
		bottomIndexLayout = (LinearLayout) bindUI(R.id.bottom_index_layout, null);
		bottomTypeLayout = (LinearLayout) bindUI(R.id.bottom_type_layout, null);
		bottomRecycleLayout = (LinearLayout) bindUI(R.id.bottom_recovery_layout, null);
		bottomMeLayout = (LinearLayout) bindUI(R.id.bottom_me_layout, null);
		bottomItems.put("MainActivity", bottomIndexLayout);
		bottomItems.put("TypeActivity", bottomTypeLayout);
		bottomItems.put("RecycleActivity", bottomRecycleLayout);
		bottomItems.put("MeActivity", bottomMeLayout);
		
		setAllBottomItem();
	}
	
	private void setAllBottomItem() {
		for (String key : bottomItems.keySet()) {
			switch (key) {
			case "MainActivity":
				setBottemItem(bottomIndexLayout, "MainActivity".equals(currActivity) ? R.drawable.star_select : null, 
						R.string.bottom_index_image_str, R.string.bottom_index_str,
						new OnClickListener() {
							@Override
							public void onClick(View v) {
								if (!"MainActivity".equals(currActivity)) {
									setMainLayout(R.layout.activity_main, false);
									setSelectStar("MainActivity");
									currActivity = "MainActivity";
								}
							}
						});
				break;

			case "TypeActivity":
				setBottemItem(bottomTypeLayout, "TypeActivity".equals(currActivity) ? R.drawable.star_select : null, 
						R.string.bottom_type_image_str, R.string.bottom_type_str, 
						new OnClickListener() {
							@Override
							public void onClick(View v) {
								if (!"TypeActivity".equals(currActivity)) {
									boolean isIn = true;
									if (!currActivity.equals("MainActivity")) {
										isIn = false;
									}
									setMainLayout(R.layout.activity_type, isIn);
									setSelectStar("TypeActivity");
									currActivity = "TypeActivity";
								}
							}
						});
				break;
				
			case "RecycleActivity":
				setBottemItem(bottomRecycleLayout, "RecycleActivity".equals(currActivity) ? R.drawable.star_select : null, 
						R.string.bottom_recovery_image_str, R.string.bottom_recovery_str,
						new OnClickListener() {
							@Override
							public void onClick(View v) {
								if (!"RecycleActivity".equals(currActivity)) {
									boolean isIn = true;
									if (currActivity.equals("MeActivity")) {
										isIn = false;
									}
									setMainLayout(R.layout.activity_recycle, isIn);
									setSelectStar("RecycleActivity");
									currActivity = "RecycleActivity";
								}
							}
						});
				break;
				
			case "MeActivity":
				setBottemItem(bottomMeLayout, "MeActivity".equals(currActivity) ? R.drawable.star_select : null, 
						R.string.bottom_me_image_str, R.string.bottom_me_str,
						new OnClickListener() {
							@Override
							public void onClick(View v) {
								if (!"MeActivity".equals(currActivity)) {
									setMainLayout(R.layout.activity_me, true);
									setSelectStar("MeActivity");
									currActivity = "MeActivity";
								}
							}
						});
				break;
				
			default:
				break;
			}
		}
	}
	
	private void setSelectStar(String selectItem) {
		for (String key : bottomItems.keySet()) {
			LinearLayout layout = bottomItems.get(key);
			ImageView imageView = (ImageView) layout.getChildAt(0);
			if (key.equals(selectItem)) {
				imageView.setImageResource(R.drawable.star_select);
			} else {
				imageView.setImageResource(R.drawable.star);
			}
		}
	}
	
	private void setBottemItem(LinearLayout bottomItem, Integer imageId, Integer imageDescriptionId, Integer textId, OnClickListener listener) {
		ImageView imageView = (ImageView) bottomItem.getChildAt(0);
		TextView textView = (TextView) bottomItem.getChildAt(1);
		if (imageDescriptionId != null) 
			imageView.setContentDescription(getResources().getString(imageDescriptionId));
		if (imageId != null) 
			imageView.setImageResource(imageId);
		if (textId != null) {
			textView.setText(getResources().getString(textId));
		}
		if (listener != null) {
			bottomItem.setOnClickListener(listener);
		}
	}
	
	
	
	/**
	 * 隐藏顶部左边
	 */
	public void topLeftLayoutHiden() {
		topLeftLayout.setOnClickListener(null);
		topLeftTextView.setText(" ");
	}
	
	/**
	 * 设置顶部左边
	 * 
	 * @param text
	 * @param listener
	 */
	public void setTopLeft(String text, OnClickListener listener) {
		topLeftLayout.setOnClickListener(listener);
		topLeftTextView.setText(text);
		@SuppressWarnings("deprecation")
		Drawable dw = getResources().getDrawable(R.drawable.icon_back);
		dw.setBounds(18, 2, 30, 20);
		topLeftTextView.setCompoundDrawables(dw, null, null, null);
	}
	
	/**
	 * 隐藏顶部右边
	 */
	public void topRightLayoutHiden() {
		topRightLayout.setOnClickListener(null);
		topRightTextView.setText(" ");
	}
	
	/**
	 * 设置顶部右边
	 * 
	 * @param text
	 * @param listener
	 */
	public void setTopRight(String text, OnClickListener listener) {
		topRightLayout.setOnClickListener(listener);
		topRightTextView.setText(text);
	}
	
	/**
	 * 绑定界面的view
	 * 
	 * @param view
	 * @param id
	 * @param l 点击事件
	 */
	public View bindUI(int id, View.OnClickListener l) {
		View view = findViewById(id);
		
		if (l != null) 
			view.setOnClickListener(l);
		
		return view;
	}
	
	/**
	 * 根据layout的id创建
	 * 
	 * @param layoutId
	 * @return
	 */
	public View getLayoutById(int layoutId) {
		LayoutInflater inflater = getLayoutInflater();
		View view = inflater.inflate(layoutId, null);
		return view;
	}
	
	/**
	 * 是否显示顶部
	 * 
	 * @param isShow[View.VISIBLE, View.INVISIBLE, View.GONE]
	 */
	public void showTop(int isShow) {
		topLayout.setVisibility(isShow);
	}
	
	/**
	 * 是否显示底部
	 * 
	 * @param isShow[View.VISIBLE, View.INVISIBLE, View.GONE]
	 */
	public void showBottom(int isShow) {
		bottomLayout.setVisibility(isShow);
		if (isShow == View.VISIBLE) {
			isShowBottom = true;
		} else if (isShow == View.GONE) {
			isShowBottom = false;
		}
	}

	/**
	 * 获取顶部的左侧layout
	 * 
	 * @return
	 */
	public LinearLayout getTopLeftLayout() {
		return topLeftLayout;
	}

	/**
	 * 获取顶部中间layout
	 * 
	 * @return
	 */
	public LinearLayout getTopCenterLayout() {
		return topCenterLayout;
	}

	/**
	 * 获取顶部右侧layout
	 * 
	 * @return
	 */
	public LinearLayout getTopRightLayout() {
		return topRightLayout;
	}

	public TextView getTopLeftTextView() {
		return topLeftTextView;
	}

	/**
	 * 获取控制顶部内容的TextView
	 * 
	 * @return
	 */
	public TextView getTopCenterTextView() {
		return topCenterTextView;
	}
	
	public void setTopCenterText(String text) {
		topCenterTextView.setText(text);
	}

	public TextView getTopRightTextView() {
		return topRightTextView;
	}
	
	/**
	 * 设置子类的主界面
	 * 
	 * @param layoutId
	 */
	public void setMainLayout(int layoutId, boolean isIn) {
		View view = viewMap.get(layoutId);
		mainLayout.removeAllViews();
		Animation animation = null;
		if (view == null) {
			view = getLayoutById(layoutId);
			viewMap.put(layoutId, view);
		} 
		if (isIn) {
			animation = AnimationUtils.loadAnimation(this, R.anim.view_in);
		} else {
			animation = AnimationUtils.loadAnimation(this, R.anim.view_out);
		}
		
		view.startAnimation(animation);
		
		mainLayout.addView(view);
	}
	
	/**
	 * 设置子类的主界面
	 * 
	 * @param layoutId
	 */
	public void setMainLayoutUseAnim(int layoutId, boolean useAnim) {
		View view = viewMap.get(layoutId);
		mainLayout.removeAllViews();
		if (view == null) {
			view = getLayoutById(layoutId);
			viewMap.put(layoutId, view);
			if (useAnim) {
				Animation animation = AnimationUtils.loadAnimation(this, R.anim.view_in);
				view.startAnimation(animation);
			}
		} else {
			if (useAnim) {
				Animation animation = AnimationUtils.loadAnimation(this, R.anim.view_out);
				view.startAnimation(animation);
			}
		}
		
		mainLayout.addView(view);
	}

	public LinearLayout getMainLayout() {
		return mainLayout;
	}
	
	/**
	 * 随机生成6位数字
	 * 
	 * @return
	 */
	public int createVerificationCode() {
		int randNum = (int) ((Math.random() * 9 + 1) * 100000);
		
		return randNum;
	}
}
