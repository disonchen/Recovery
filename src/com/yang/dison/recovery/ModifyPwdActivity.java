package com.yang.dison.recovery;

import android.os.Bundle;
import android.view.View;

import com.yang.dison.recovery.core.ui.RecoveryActivity;

public class ModifyPwdActivity extends RecoveryActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		initView();
	}
	
	private void initView() {
		setMainLayoutUseAnim(R.layout.activity_modifypwd, false);
		setTopCenterText("修改密码");
		showBottom(View.GONE);
	}
}
