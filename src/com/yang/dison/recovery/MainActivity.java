package com.yang.dison.recovery;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.yang.dison.recovery.core.ui.RecoveryActivity;
import com.yang.dison.recovery.core.ui.SlideShowView;

public class MainActivity extends RecoveryActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setMainLayoutUseAnim(R.layout.activity_main, false);
//		topLeftLayoutHiden();
//		topRightLayoutHiden();
		setTopRight("登录", new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MainActivity.this, LoginActivity.class);
				startActivity(intent);
			}
		});
		
		String[] imageUrls = new String[]{
				"http://image.zcool.com.cn/56/35/1303967876491.jpg",  
				"http://image.zcool.com.cn/47/19/1280115949992.jpg", 
                "http://image.zcool.com.cn/59/54/m_1303967870670.jpg",  
                "http://image.zcool.com.cn/47/19/1280115949992.jpg",  
                "http://image.zcool.com.cn/59/11/m_1303967844788.jpg"
		};
		
		SlideShowView view = (SlideShowView) findViewById(R.id.slideshowView);
		view.setImageUrls(imageUrls);
	}
	
}
